
<div id="wrapper">
    <div id="left_panel">
        <div id="m_head">
            <div id="page_title">
                <form method="post">
                    <label class="marsh">Какой маршрут</label>
                    <input class="marsh" type="text" size="2" name="marsh" value="<?=implode(', ', $mts ?? '')?>" required>
                    <button>Давай</button>
                </form>
                <div><?=implode(', ', $mts ?? '')?></div>
            </div>
            <div id="legend">
                <table>
                    <tr>
                        <td id="leg" class="col" colspan="2">Легенда:</td>
                    </tr>
                    <tr>
                        <td class="col morning">1 заезд</td>
                        <td class="col evening">2 заезд</td>
                    </tr>
                </table>
            </div>
        </div>

        <?if(!empty($mts)):?>
        <?foreach($mts as $key => $m_t):?>

        <?if(!empty(trim($m_t))):?>

        <div class="list">
            <table onmouseleave="dropzoom()">
                <thead>
                    <tr>
                        <td class="marsh_num mt<?=$m_t?>"><?=$m_t?></td>
                        <?foreach($marsh[$key][0] as $mt => $val):?>
                            <?if($mt != 'timebud' && $mt != 'nmag'):?>
                            <td class="col marsh_num mt<?=$m_t?>"><?=$mt?></td>
                            <?endif?>
                        <?endforeach?>
                    </tr>
                </thead>

                <?foreach($marsh[$key] as $inkey => $mt):?>

                <tr class="line" onmouseenter="mapfly(<?=$mt_adresses[$key][$inkey]['latitude']?>, <?=$mt_adresses[$key][$inkey]['longitude']?>)">
                    <td class="marsh_num mt<?=$m_t?>"><?=++$counter?></td>
                    <?foreach($mt as $cell => $val):?>
                        <?if($cell != 'timebud' && $cell != 'nmag'):?>
                        <td class="col <?=$mt['timebud'] == 2 ? 'evening' : 'morning' ?>"><?=$val?></td>
                        <?endif?>
                    <?endforeach?>
                </tr>

                <?endforeach?>
                <?$counter = 0;?>

            </table>
        </div>
        <?endif?>
        <?endforeach?>
        <?endif?>
    </div>
    <div id="mapid"></div>

    <div id="disclaimer">
        <div id="welcome">Вас категорически приветствует тестовая программа "БРЫСЬ", версия 0.2<br>
        Слеплена "на коленке из того что было"</div>
        <ul>
            <li>В качестве сервера используется лично мой ноутбук, поэтому она не будет работать если он выключен</li>
            <li>Представляет из себя визуальное представление точек маршрутов на карте</li>
            <li>Из интернета подгружаются:
                <ul>
                    <li>Изображения карты (возможно реализовать локальный сервер, приблизительно 100Гб информации для СПб и ЛО)</li>
                    <li>Координаты неизвестных адресов, далее они сохраняются в локальной базе данных (возможно реализовать локально)</li>
                    <li>Маршрутизация (на текущий момент отключил, потребляет очень большое количество запросов к серверам географических провайдеров, возможно реализовать локально)</li>
                </ul>
            </li>
            <li>Используемые данные - импортированная наша база, Невский участок, срез - февраль 2017</li>
            <li>Координаты точек получены автоматически от провайдеров Яндекс, Google, OpenStreetMap (в порядке приоритета,
                в случае отсутствия ответа от одного, используется следующий)</li>
            <li>Некоторые, особо выделяющиеся своей некорректностью, адреса были подправлены лично мной (порядка 50 штук)
                чтобы координаты были определены более верно</li>
            <li>В случае если в карточке клиента был пустой адрес, точка будет иметь координаты "Книпович 21"</li>
            <li>В случае если ни один географический провайдер не смог определить координаты, точка будет иметь координаты
                центра Дворцовой площади</li>
            <li>Используемый провайдер карт - OpenStreetMap</li>
            <li>Все географические провайдеры имеют ограничения на количество запросов (обращений) в сутки к их серверам т.к. используются бесплатно</li>
            <li>Можно вводить сразу несколько маршрутов (из списка существующих на Невском), через ЗАПЯТУЮ, хоть все</li>
            <li>В приложении возможны ошибки (проверялась лишь базовая работоспособность), лучше о них сообщать</li>
            <li>Насыщенность цвета точки на карте зависит от количества карточек в месте этой точки</li>
            <li>При наведении курсора мыши на строку с клиентом маршрута карта "перелетит" на ее позицию</li>
        </ul>

        <div id="close"><a href="javascript:void(0);" onclick="viewdiv('disclaimer');">Закрыть блок</a></div>
    </div>
</div>
<script src="/lib/leaflet/leaflet.js"></script>
<script src="/lib/leaflet-control-geocoder-1.5.4/dist/Control.Geocoder.js"></script>
<script src="/lib/leaflet-routing-machine-3.2.5/dist/leaflet-routing-machine.js"></script>
<script>
    function viewdiv(id) {
        var el = document.getElementById(id);
        el.style.display = "none";
        console.log(id, 'closed');
    }
    function mapfly(lat, lng){
        //var mmap = document.getElementById('mapid');
        map.flyTo([lat, lng], 18);
        //document.getElementById('leg').innerHTML = 'fly';
        console.log('flying to ', lat, lng)
    }

    function dropzoom(){
        map.fitBounds(group0.getBounds());
    }

    <?if(!empty($mt_adresses)):?>
    <?php
    function markerCreator($lat, $long, $label, $num, $key, $mt) {
        return "var myIcon = L.divIcon({iconSize: new L.Point(25, 25), className: 'pin mt{$mt}', html: $num});
        var marker_{$key}_{$num} = L.marker([$lat, $long], {title: \"Точка №$num: $label\", icon: myIcon, draggable: true}).addTo(map);
        marker_{$key}_{$num}.bindPopup(\"Точка №$num: $label\");";
    }
    function wpCreator($lat, $long) {
        return "L.latLng($lat, $long)";
    }

    function polyCreator($lat, $long) {
        return "[$lat, $long]";
    }
    foreach($mt_adresses as $key => $mt_addr) {
        foreach ($mt_addr as $cnt => $point) {
            $num = $cnt + 1;
            $mapdata[$key][] = markerCreator($point['latitude'], $point['longitude'], $point['fox_adr'], $num, $key, $mts[$key]);
            //$mapwp[$key][] = wpCreator($point['latitude'], $point['longitude']);
            $mappoly[$key][] = polyCreator($point['latitude'], $point['longitude']);
            $marker_group[$key][] = "marker_{$key}_{$num}";
        }
    }



    ?>

    if (L.Browser.ielt9) {
        alert('Используемый браузер не поддерживается, корректная работа приложения не гарантируется!');
    }

    var map = L.map('mapid').setView([59.90159, 30.386943], 13);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> Тестовая версия системы "БРЫСЬ" (версия 0.2) &copy; Пикулик А.А.'
    }).addTo(map);

    //var wp = [< /*implode(', ', $mapwp[0]);*/ ];

    //    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2lmdDg1IiwiYSI6ImNqMWxwbjhvaTAwMGoyeXIzdnNnZm55cHcifQ.ZxSUJZ-MUkRl3aaJKOXAxw', {
    //        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    //        maxZoom: 18,
    //        id: 'mapbox.streets',
    //        accessToken: 'pk.eyJ1IjoiZ2lmdDg1IiwiYSI6ImNqMWxwbjhvaTAwMGoyeXIzdnNnZm55cHcifQ.ZxSUJZ-MUkRl3aaJKOXAxw'
    //    }).addTo(map);

    <? foreach($mapdata as $key => $mdata): ?>
    <?= implode('', $mdata);?>
    var group<?=$key?> = new L.featureGroup([<?=implode(', ', $marker_group[$key]);?>]);
    <?endforeach?>
    map.fitBounds(group0.getBounds());

    <?endif?>
    //L.Control.geocoder().addTo(map);

    //geocoder = new L.Control.Geocoder.nominatim();

    // function onMapClick(e) {
    //     alert("You clicked the map at " + e.latlng);
    // }

    //map.on('click', onMapClick);



//         L.Routing.control({
//             waypoints: wp,
//             routeWhileDragging: true,
//             /*geocoder: L.Control.Geocoder.nominatim(),*/
//             /*geocoder: L.Control.Geocoder.bing(),*/
//             geocoder: L.Control.Geocoder.google(),
//             language: 'en'
//         }).addTo(map);
//
//        function createButton(label, container) {
//            var btn = L.DomUtil.create('button', '', container);
//            btn.setAttribute('type', 'button');
//            btn.innerHTML = label;
//            return btn;
//        }

    //    map.on('click', function(e) {
    //        var container = L.DomUtil.create('div'),
    //            startBtn = createButton('Start from this location', container),
    //            destBtn = createButton('Go to this location', container);
    //
    //        L.popup()
    //            .setContent(container)
    //            .setLatLng(e.latlng)
    //            .openOn(map);
    //    });

    //     document.getElementById('leg').innerHTML = results[0].center.lat;



</script>

