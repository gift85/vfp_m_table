/**
 * Created by ANDREW-LH532 on 17.04.2017.
 */
var mymap = L.map('mapid').setView([59.90159, 30.386943], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2lmdDg1IiwiYSI6ImNqMWxwbjhvaTAwMGoyeXIzdnNnZm55cHcifQ.ZxSUJZ-MUkRl3aaJKOXAxw', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZ2lmdDg1IiwiYSI6ImNqMWxwbjhvaTAwMGoyeXIzdnNnZm55cHcifQ.ZxSUJZ-MUkRl3aaJKOXAxw'
}).addTo(mymap);

function onMapClick(e) {
    alert("You clicked the map at " + e.latlng);
}

mymap.on('click', onMapClick);