<?php
return [
    'Core\User' => [
        'dependency' => [
            'db' => 'service:SQL',
            'mUser' => 'model:User',
            'mSession' => 'model:Session'
        ]
    ],
	'Core\Sql' => [
	    'dependency' => [
	        'config' => 'service:Config'
        ]
    ],
    'Core\Config' => [
        'settings' => [
            'db' => [
                'name' => 'fox_nev',
                'user' => 'root',
                'pass' => '',
                'host' => '10.1.55.98',/*'localhost',*/
                'driver' => 'mysql'
            ],
            'base' => [
                'path' => __DIR__ . DIRECTORY_SEPARATOR,
                'tmp_path' => __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR,
                'logs_path' =>  __DIR__ . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR,
            ]
        ]
    ],
    'models\User' => [
        'dependency' => [
            'db' => 'service:SQL'
        ]
    ],
    'models\Session' => [
        'dependency' => [
            'db' => 'service:SQL'
        ]
    ],
    'Dbf_importer' => [
        'dependency' => [

        ]
    ],
    'Marsh_map_app' => [
        'dependency' => [
            'db' => 'service:Sql',
            'request' => 'service:Request'
        ]

    ]
];
