<?php
spl_autoload_register(function ($classname){
    $classname = strtolower($classname);
    $classname = str_replace('\\', '/', $classname);
    /*echo $classname . '.php<br>';*/
    include_once($classname . '.php');
});