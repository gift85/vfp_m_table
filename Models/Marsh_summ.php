<?php
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 16.04.2017
 * Time: 18:00
 */

namespace Models;

use Core\Sql;

class Marsh_summ extends Model
{
    public function __construct(Sql $db)
    {
        parent::__construct($db);
        $this->table = 'summ';
        $this->primary_key = 'nkart';//?
    }

    public function validation_rules()
    {
        // TODO: Implement validation_rules() method.
    }

    public function get_nkart_summ($nkart = '', $cur_date_lim = '')
    {
        $cur_date_lim = $cur_date_lim ?? '2016-12-31';
        $sql = "SELECT nkart, cur_date, marsh, nomsv FROM {$this->table} WHERE nkart = :nkart and cur_date > :cur_date_lim ORDER BY cur_date" ;

        $params = compact('nkart', 'cur_date_lim');

        return $this->db->select($sql, $params);
    }
}