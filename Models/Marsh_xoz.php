<?php
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 16.04.2017
 * Time: 18:00
 */

namespace Models;

use Core\Sql;

class Marsh_xoz extends Model
{
    public function __construct(Sql $db)
    {
        parent::__construct($db);
        $this->table = 'xoz';
        $this->primary_key = 'nkart';//?
    }

    public function validation_rules()
    {
        // TODO: Implement validation_rules() method.
    }

    public function get_bud_marsh($marsh = '')
    {
        $sql = "SELECT nkart, vzaezd, nmag, torg, concat_ws(' ', adres, dom) as adres, timebud FROM {$this->table} WHERE marsh = :marsh and deleted = 0 ORDER BY vzaezd" ;

        $params = compact('marsh');

        return $this->db->select($sql, $params);
    }
}