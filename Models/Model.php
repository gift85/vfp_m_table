<?php

namespace Models;

use Core\Sql;

abstract class Model{
    protected $db;
    protected $table;
    protected $primary_key;

    protected function __construct(Sql $db)
    {
        $this->db = $db;
    }

    public abstract function validation_rules();

    public function get_all():array
    {
        return $this->db->select("SELECT * FROM {$this->table}");
    }

    public function get_one(string $primary_key):array
    {
        $sql = "SELECT * FROM {$this->table} WHERE {$this->primary_key} = :primary_key";
        $params = compact('primary_key');

        return $this->db->select($sql, $params);
    }

    public function remove(string $primary_key):int
    {
        $params = compact('primary_key');
        $filter = "{$this->primary_key} = :primary_key";

        return $this->db->delete($this->table, $filter, $params);
    }

    public function add(array $params):int
    {
        $map = $this->validation_rules();

        foreach($params as $key => $value){
            if(!in_array($key, $map['fields'])){
                throw new \Exception("incorrect field $key");
            }
        }

        return $this->db->insert($this->table, $params);
    }

    public function update(string $primary_key, array $set):int
    {
        $filter = "{$this->primary_key} = :primary_key";
        $params = compact('primary_key');

        return $this->db->update($this->table, $set, $filter, $params);
    }
}
