<?php
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 16.04.2017
 * Time: 18:00
 */

namespace Models;

use Core\Sql;

class Views extends Model
{
    public function __construct(Sql $db)
    {
        parent::__construct($db);
        $this->table = 'views';
        $this->primary_key = 'id';//?
    }

    public function validation_rules()
    {
        return [
            'fields' => [
                'id',
                'ip',
                'last_view'
            ]
        ];
    }

    public function get_client_ip($ip)
    {
        $sql = "SELECT ip FROM {$this->table} WHERE ip = :ip" ;

        $params = compact('ip');
        return $this->db->select($sql, $params);
    }

    public function set_client_ip($ip)
    {
        return $this->add(['ip' => $ip]);
    }

    public function up_client_ip($client_ip)
    {
        return $this->update($client_ip, ['last_view' => time()]);
    }
}