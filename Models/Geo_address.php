<?php
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 16.04.2017
 * Time: 18:00
 */

namespace Models;

use Core\Sql;

class Geo_address extends Model
{
    public function __construct(Sql $db)
    {
        parent::__construct($db);
        $this->table = 'adr2coords';
        $this->primary_key = 'id';//?
    }

    public function validation_rules()
    {
        return [
            'fields' => [
                'fox_adr',
                'latitude',
                'longitude',
                'south_latitude',
                'west_longitude',
                'north_latitude',
                'east_longitude',
                'street_name',
                'street_number',
                'sub_locality',
                'locality',
                'postal_code',
                'provider'
            ],
            'not_empty' => ['fox_adr', 'latitude', 'longitude', 'provider']
        ];
    }

    public function get_coordinates($adres)
    {
        $sql = "SELECT fox_adr, latitude, longitude FROM {$this->table} WHERE fox_adr = :adres" ;

        $params = compact('adres');
        return $this->db->select($sql, $params);
    }

    public function set_coordinates($point)
    {
        return $this->add($point);
    }
}