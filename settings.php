<?php
return [
    'db' => [
        'name' => 'fox_nev',
        'user' => 'root',
        'pass' => '',
        'host' => '10.1.55.98',
        'driver' => 'mysql'
    ],
	'base' => [
		'path' => __DIR__ . DIRECTORY_SEPARATOR,
        'tmp_path' => __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR
	]
];
