<?php

namespace Dbf_importer;

use Inok\Dbf2mysql\Convert;
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 15.04.2017
 * Time: 22:31
 */
class Dbf_importer
{
    public $db_host = "10.1.55.98";/*"localhost";*/
    public $db_port = "3306";
    public $db_username = "root";
    public $db_password = "";
    public $db_name = "fox_nev";
    public $db_charset = "utf-8";
    public $dbf_path = "F:\\bd_ink_copy\\ink\\";
    public $dbf_list = null;
    public $table_prefix = null;
    public $key_field = null;
    public $column_only = false;
    public $deleted_records = true;
    public $verbose = true;
    public $log_path = "/dbf2mysql.log";
    private $config;

    public function init()
    {
        $this->config = [
            "db_host" => $this->db_host,
            "db_port" => $this->db_port,
            "db_username" => $this->db_username,
            "db_password" => $this->db_password,
            "db_name" => $this->db_name,
            "db_charset" => $this->db_charset,
            "dbf_path" => $this->dbf_path,
            "dbf_list" => $this->dbf_list,
            "table_prefix" => $this->table_prefix,
            "key_field" => $this->key_field,
            "column_only" => $this->column_only,
            "deleted_records" => $this->deleted_records,
            "verbose" => $this->verbose,
            "log_path" => $this->log_path,
        ];

    }

    public function convert()
    {
        set_time_limit(1230);
        echo 'converting ' . $this->dbf_list . '<br>';
        new Convert($this->config);
        echo 'convert is done <br>';
    }
}