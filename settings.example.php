<?php
return [
    'db' => [
        'name' => 'db_name',
        'user' => 'user',
        'pass' => 'password',
        'host' => 'localhost',
        'driver' => 'mysql'
    ],
	'base' => [
		'path' => __DIR__ . DIRECTORY_SEPARATOR,
        'tmp_path' => __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR
	]
];
