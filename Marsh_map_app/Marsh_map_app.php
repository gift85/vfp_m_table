<?php

namespace Marsh_map_app;

use Dbf_importer\Dbf_importer;
use Core\Response;
use Models\{Marsh_xoz, Geo_address, Views};

class Marsh_map_app
{
    private $db;
    public $request;
    private $geodb;
    private $geocoder = null;
    private $client_ip;

    public function __construct($sql, $request)
    {
        $this->db = $sql;
        $this->request = $request;
        //set used table names

        $this->client_ip = $_SERVER['HTTP_X_REAL_IP'];

        $iview = new Views($this->db);

        //if (empty($iview->get_client_ip($this->client_ip)) ){
         //   $iview->set_client_ip($this->client_ip);
        //}else{
            $iview->set_client_ip($this->client_ip);
        //}
    }

    public function import_dbf()
    {
        $idbf = new Dbf_importer();
        $idbf->dbf_path = "F:\\bd_ink_copy\\ink\\oi\\dbf";
        $idbf->dbf_list = ["xoz"];
        $idbf->init();
        $idbf->convert();
    }

    public function show($app)
    {
        $marsh = [];
        $mts = $this->request['marsh'];
        $mts = explode(',', $mts);
        //$mt = htmlspecialchars($mt);
        //var_dump($mts);
        $marsh_xoz = new Marsh_xoz($this->db);
        $this->geodb = new Geo_address($this->db);
        foreach($mts as $key => $mt){
            $mts[$key] = htmlspecialchars($mt);
            $mts[$key] = trim($mts[$key]);
            $mt = $mts[$key];
            if(!empty($mt)){

                $marsh[$key] = $marsh_xoz->get_bud_marsh($mt);
                $mt_adresses[$key] = $this->geo_prepare($marsh[$key]);
                if(empty($mt_adresses[$key])){
                    unset($mts[$key]);
                    unset($mt_adresses[$key]);
                };
            }
        }
        //var_dump($mt_adresses,$marsh);

        (new Response($app))
            ->addTmp('m_base.html', ['mts' => $mts, 'mt_adresses' => $mt_adresses])
            ->addTmp('m_table.html', ['mts' => $mts, 'marsh' => $marsh, 'mt_adresses' => $mt_adresses], 'm_table')
            ->render();
    }

    private function geo_prepare($marsh)
    {
        $mt_adresses = array_column($marsh, 'adres');//, 'nkart');//, 'vzaezd');

        $mt_adresses = array_map(function ($adr) {
                if(empty(trim($adr))){
                    $adr = 'ул. Книпович д. 21';
                }
                return "Санкт-Петербург $adr";
            }, $mt_adresses);
        //var_dump($mt_adresses);

        $coords = [];

        foreach($mt_adresses as $adr){
            if(empty(trim($adr))){
                $adr = 'Санкт-Петербург ул. Книпович д. 21';
            }
            $res = $this->get_coordinates($adr);
            $coords[] = $res;
        }

        //var_dump($coords, $marsh, $mt_adresses);
        //var_dump(json_encode($coords));
        //var_dump($coords);
        return $coords;
    }

    private function get_coordinates($address)
    {
        $address = preg_replace("/\s{2,}/"," ", $address);
        $address = mb_strtolower($address);
        $address = str_replace('"', '', $address);

        /*address with trimmed useful spaces!*/
        $res = $this->geodb->get_coordinates($address)[0];

        if(empty($res)){
            echo "geoservice used for $address!<br>";
            $res = $this->geocode($address);
            $this->geodb->set_coordinates($res);
        }else{
            //echo "geoservice not used for $address!<br>";
        }

        return $res;
    }

    private function geocode($address){
        if($this->geocoder === null) {
            $adapter = new \Ivory\HttpAdapter\CurlHttpAdapter();
            $this->geocoder = new \Geocoder\ProviderAggregator();
            $chain = new \Geocoder\Provider\Chain([
                new \Geocoder\Provider\Yandex($adapter),
                new \Geocoder\Provider\GoogleMaps($adapter),
                new \Geocoder\Provider\OpenStreetMap($adapter)
            ]);

            $this->geocoder->registerProvider($chain);
        }

        //Geocoder\Exception\NoResult
        //Geocoder\Exception\QuotaExceeded

        try {
            $res = $this->geocoder->geocode($address);
        } catch(\Geocoder\Exception\NoResult $e){
            throw new \Exception('provider not working, trying next point..<br>');//need to catch!!
        }

        $res = $res->first()->toArray();

        $point = [
            'fox_adr' => $address,
            'latitude' => $res['latitude'],
            'longitude' => $res['longitude'],
            'south_latitude' => $res['bounds']['south'],
            'west_longitude' => $res['bounds']['west'],
            'north_latitude' => $res['bounds']['north'],
            'east_longitude' => $res['bounds']['east'],
            'street_name' => $res['streetName'],
            'street_number' => $res['streetNumber'],
            'sub_locality' => $res['subLocality'],
            'locality' => $res['locality'],
            'postal_code' => $res['postalCode'],
            'provider' => 'chain',/*need to know what provider used*/
        ];

        return $point;
    }
}

