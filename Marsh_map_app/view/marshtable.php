<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?=$page_title?></title>
    <link rel="stylesheet" type="text/css" href="show.css">

</head>
<body>
    <div id="wrapper">
        <div id="page_title">
            <h1><?=$page_title?></h1>
            <form method="post">
                <label class="marsh">Какой маршрут</label>
                <input class="marsh" type="text" size="2" name="marsh" value="<?=$mt ?? ''?>" required>
                <button>Давай</button>
            </form>
        </div>
        <table>
            <tr>
                <td class="col" colspan="2">Легенда:</td>
            </tr>
            <tr>
                <td class="col morning">1 заезд</td>
                <td class="col evening">2 заезд</td>
            </tr>
        </table>

        <hr>
        <?if(isset($mt)):?>

        <div class="list">
            <div id="list"></div>
            <table>
            <tr>
                <td></td>
                <?foreach($marsh[0] as $mt=>$val):?>
                    <?if($mt != 'timebud'):?>
                    <td class="col"><?=$mt?></td>
                    <?endif?>
                <?endforeach?>
            </tr>
            <?foreach($marsh as $mt):?>
                <tr>
                    <td><?=++$counter?></td>
                    <?foreach($mt as $cell=>$val):?>
                        <?if($cell != 'timebud'):?>
                        <td class="col <?=$mt['timebud'] == 2 ? 'evening' : 'morning' ?>"><?=$val?></td>
                        <?endif?>
                    <?endforeach?>

                    <?//$summ = get_summ_from_db($mt['nkart']);?>
                    <?//var_dump($mt['nkart']);?>
                    <?//var_dump($summ);?>
                    <?//if(is_array($summ)):?>

                        <!-- <?//for($i = 1; $i < 13; $i++):?>

                            <td class="col green"></td>
                        <?//else:?>
                            <td class="col white"></td>

                        <?//endfor?> -->


                    <?//endif?>
                </tr>
            <?endforeach?>
            </table>
        </div>
        <?endif?>

        <div id="map">

        </div>
        <hr>

    </div>
    <!-- <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//yandex.st/jquery/2.2.3/jquery.min.js" type="text/javascript"></script> -->
    <!-- <script>
       //require "marshtable/view/route.js";
    </script> -->
</body>
</html>
