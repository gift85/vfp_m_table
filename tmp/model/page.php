<?php

namespace Model;

use Core\Traits\Singleton;
use Core\Model;

class Page extends Model{
    use Singleton;

    protected function __construct(){
        parent::__construct();
        $this->table = 'articles';
        $this->primary_key = 'article_id';
    }

    public function validation_rules(){
        return [
            'fields' => ['article_id', 'user_id', 'created', 'title', 'content'],
            'not_empty' => ['article_id', 'user_id', 'title', 'content'],
            'allow_html' => ['title', 'content']
        ];
    }

    public function get_index():array{
        $sql = "SELECT article_id, title FROM articles ORDER BY created DESC";/*LIMIT :start, 10*/

        return $this->db->select($sql);
    }

    public function get_article(string $title_id):array{
        $sql = "SELECT title, content, created, name as user, user_id FROM articles LEFT JOIN users using(user_id) WHERE article_id = :title_id";
        $params = compact('title_id');

        return $this->db->select($sql, $params);
    }
}
