<?php

namespace Core;

class Validation{
    protected $data;
    protected $rules;
    protected $errors;
    protected $clean_data;

    public function __construct(array $data, array $rules){
        $this->data = $data;
        $this->rules = $rules;
        $this->errors = [];
    }

    public function execute():void{
        foreach($this->data as $key => $value){
            $value = trim($value);

            if(in_array($key, $this->rules['not_empty']) && $value == ''){
                $this->errors[] = "Field $key can't be empty.";
            }
            else{
                if(!in_array($key, $this->rules['allow_html'])){
                    $value = htmlspecialchars($value);
                }
            }
            $this->clean_data[$key] = $value;
        }
    }

    public function good():bool{
        return empty($this->errors);
    }

    public function clean_data():array{
        return $this->clean_data;
    }

    public function errors():array{
        return $this->errors;
    }

}
