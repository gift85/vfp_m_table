<?php

namespace Core;

use PDO, PDOStatement, PDOException, Exception;
/**
 * Created by PhpStorm.
 * User: ANDREW-LH532
 * Date: 14.04.2017
 * Time: 13:45
 */
class Sql extends Container
{
    protected $conn = null;

    public function init(Config $params)
    {
        $host = $params->getSetting('db.host');
        $dbname = $params->getSetting('db.name');
        $user = $params->getSetting('db.user');
        $pass = $params->getSetting('db.pass');

        try{
            $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass, [
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                /*PDO::ATTR_PERSISTENT => true*/
            ]);
        }
        catch (PDOException $e){
            throw new Exception("Connection error {$e->getMessage()}");
        }
        //echo "db init <br>";
    }

    public function select(string $sql, array $params = []):array
    {
        $query = $this->execute_query($sql, $params);
        return $query->fetchAll();
    }

    public function insert(string $table, array $params):int
    {
        $keys = array_keys($params);
        $masks = array_map(function (string $key){
            return ":$key";
        }, $keys);

        $fields = implode(', ', $keys);
        $values = implode(', ', $masks);

        $sql = "INSERT INTO $table ($fields) VALUES ($values)";
        $query = $this->execute_query($sql, $params);
        return $this->conn->lastInsertId();/*$query->rowCount();/*$this->db->lastInsertId();/*или rowCount()?*/
    }

    public function update(string $table, array $set, string $filter, array $params = []):int
    {
        $keys = array_keys($set);
        $masks = array_map(function ($key){
            return "$key = :$key";
        }, $keys);
        $fields = implode(', ', $masks);

        $merged_params = array_merge($set, $params);
        $sql = "UPDATE $table SET $fields WHERE $filter";
        $query = $this->execute_query($sql, $merged_params);
        return $query->rowCount();
    }

    public function delete(string $table, string $filter, array $params = []):int
    {
        $sql = "DELETE FROM $table WHERE $filter";
        $query = $this->execute_query($sql, $params);
        return $query->rowCount();
    }

    protected function execute_query(string $sql, array $params):PDOStatement
    {
        $query = $this->conn->prepare($sql);
        try{
            $query->execute($params);
        }
        catch(PDOException $e){
            throw new Exception("ERROR {$e->getMessage()}");
        }
        return $query;
    }
}