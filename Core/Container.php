<?php

namespace Core;

/**
 * Class Container
 * наследуюясь от него получаем доступ к оъекту как к массиву
 * @package Core
 */
class Container implements \ArrayAccess
{
    protected $container = [];

    public function __construct($array = null)
    {
        if ($array !== null) {
            $this->container = $array;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        if ($offset === null) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }
}