<?php

namespace Core;

use Core\Exceptions\PageNotFoundException;

class Router extends Container
{
    private $storage = [];

    public function setRoute($route, $callback, $method)
    {
        $route = $this->routeHandler($route);

        $this->storage[$method][$route['mask']]['callback'] = $callback;
        $this->storage[$method][$route['mask']]['params'] = $route['params'];
    }

    public function getRoute()
    {
        return $this->rebuildRoute($this['service.provider']->getService('Request')->getUri());
    }

    private function rebuildRoute($route)
    {
        $route = $this->splitQuery($route);

        $routePartial = explode('/', $this->sliceLastDelimetr($route['route']));

        $wantedRoute = false;
        $buffer = [];

        do {
            $a = array_shift($routePartial);

            $buffer[] = $a;
            $mask = implode('/', $buffer) . count($routePartial);
            $method = $this['service.provider']->getService('Request')->methodIs();

            if (isset($this->storage[$method][$mask])) {
                $wantedRoute = $this->storage[$method][$mask];
                $wantedRout['params'] = $routePartial;
            }

        } while ($a !== null);

        if (!$wantedRoute) {
            throw new PageNotFoundException();
        }

        return $wantedRoute;
    }

    private function routeHandler($route)
    {
        $routePartial = explode('/', $route);
        $withoutParams = [];
        $params = [];

        foreach ($routePartial as $key => $value) {
            if (strpos($value, '}')) {
                $params[] = substr($value, 1, -1);
            } else {
                $routePartial[$key] = $value;
                $withoutParams[$key] = $value;
            }
        }

        return [
            'params' => $params,
            'mask' => implode('/', $withoutParams) . count($params)
        ];
    }

    private function splitQuery($route)
    {
        $buffer = explode('?', $route);

        return [
            'route' => $buffer[0],
            'parameters' => $buffer[1] ?? null
        ];
    }

    private function sliceLastDelimetr($route)
    {
        if ($route[-1] === '/') {
            return substr($route, 0, -1);
        }

        return $route;
    }
}