<?php

namespace Core\Exceptions;

class E404 extends Base{

    public function __construct($message = "", $code = 0, Exception $previous = null){
        $this->path .= 'e404/';
        parent::__construct($message, $code, $previous);
    }
}
