<?php

namespace Core;

class ServiceProvider
{
    private const SERVICE_NAMESPACE = 'Core\\';
    private const MODEL_NAMESPACE = 'Models\\';

    private $instanceStorage = [];
    private $app;

    public function __construct(App $app)
    {
        $this->app = $app;
        //var_dump($app['settings']);
        //$this->app['Config'] = $this->getService('Config');//, [$app['settings']]);
    }

    public function getService($name, array $params = [])
    {
        $name = $this->getFullyClassName($name);

        if (!isset($this->instanceStorage[$name])) {
            $this->instanceStorage[$name] = new $name;
            $this->instanceStorage[$name]['service.provider'] = $this;

            if (method_exists($this->instanceStorage[$name], 'init')) {
                $params[] = $this->injectDependency($name);

                call_user_func_array([$this->instanceStorage[$name], 'init'], ...$params);
                //$this->instanceStorage[$name]->init(...$params);
            }
        }

        return $this->instanceStorage[$name];
    }

    private function getFullyClassName($name)
    {
        return self::SERVICE_NAMESPACE . $name;
    }

    private function injectDependency(string $service)
    {
        //get array services.php
        $services = $this->app['settings'];
        $params = [];

        //check isset service name key in that array
        if (array_key_exists($service, $services)){

            //get service setting array
            $depend = $services[$service];

            //foreach array key [dependency] key => value
            if (array_key_exists('dependency', $depend)) {

                foreach ($depend['dependency'] as $key => $value) {

                    //  explode ':' value
                    list($space, $class) = explode(":", $value);

                    switch ($space) {
                        case 'model':

                            //  case model -> params[] = new model
                            $class = self::MODEL_NAMESPACE . $class;

                            $m_depend = $this->injectDependency($class);

                            $model = (new \ReflectionClass($class))->newInstanceArgs($m_depend);

                            $params[$key] = $model;
                            break;

                        case 'service':

                            //  case service -> params[] = getservice[name]
                            $params[$key] = $this->getService($class);
                            break;

                        default:

                    }
                }
            }
            if (array_key_exists('settings', $depend)) {
                $params['settings'] = $depend['settings'];
            }
        }
        // return params[]
        return $params;
    }
}