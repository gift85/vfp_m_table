<?php

namespace core;

use models\User as mUser;
use models\Session as mSession;

class User extends Container
{
    private $token;                // идентификатор текущей сессии
    private $uid;                // идентификатор текущего пользователя
    private $onlineMap;            // карта пользователей online
    private $mSessions;            // модель для работы с сессиями
    private $mUsers;
    private $table = 'users';
    private $db;
    private $privs_cache = [];

    public function init(SQl $db, mUser $user, mSession $session)
    {
        $this->db = $db;
        $this->mUsers = $user;
        $this->mSessions = $session;
    }

    /**
     * Авторизация
     * @param string $login 	- логин
     * @param string $password 	- пароль
     * @param bool $remember 	- нужно ли запомнить в куках
     * @return bool   результат	- true или false
     */
    public function Login(string $login, string $password, $remember = false):bool
    {
        // вытаскиваем пользователя из БД
        $user = $this->GetByLogin($login);

        if ($user === null) {
            return false;
        }

        $id_user = $user['id_user'];

        // проверяем пароль
        if ($user['pass'] != $this->get_hash($password)) {
            return false;
        }

        // запоминаем имя и md5(пароль)
        if ($remember) {
            $expire = time() + 3600 * 24 * 100;
            setcookie('login', $login, $expire);
            setcookie('pass', $this->get_hash($password), $expire);
        }

        // открываем сессию и запоминаем SID
        $this->token = $this->OpenSession($id_user);

        return true;
    }

    /**
     * Выход
     */
    public function Logout():void
    {
        setcookie('login', '', time() - 1);
        setcookie('pass', '', time() - 1);
        unset($_COOKIE['login']);
        unset($_COOKIE['pass']);
        unset($_SESSION['sid']);
        $this->token = null;
        $this->uid = null;
    }

    /**
     * Получение пользователя
     * $id_user		- если не указан, брать текущего
     * @return array | null результат	- объект пользователя
     */
    public function Get()
    {
        $id_user = $this->GetUid();

        if ($id_user === null)
            return null;

        // А теперь просто возвращаем пользователя по id_user.
        return $this->mUsers->one($id_user);
    }

    /**
     * Получает пользователя по логину
     * @param string $login
     * @return array | null
     */
    public function GetByLogin($login)
    {
        $query = "SELECT * FROM {$this->table} WHERE login = :login";
        $result = $this->db->select($query, ['login' => $login]);

        if (empty($result)) {
            return null;
        }

        return $result[0];
    }

    /**
     * Проверка наличия привилегии
     * @param string $priv 		- имя привилегии
     * @param string $id_user		- если не указан, значит, для текущего
     * @return bool результат	- true или false
     */
    public function Can($priv, $id_user = null):bool
    {
        if ($id_user === null)
            $id_user = $this->GetUid();

        if ($id_user === null)
            return false;

        if (!isset($this->privs_cache[$priv][$id_user])) {
            $t = "SELECT count(*) as cnt FROM  " . SQL_PREFIX . "_priv2role
				  LEFT JOIN  {$this->table} u using (id_role)
				  LEFT JOIN  " . SQL_PREFIX . "_privilege p using(id_priv) 
				  WHERE u.id_user = '%d' AND p.name = '%s'";

            $query = sprintf($t, $id_user, $priv);
            $result = $this->db->Select($query);
            $this->privs_cache[$priv][$id_user] = ($result[0]['cnt'] > 0);
        }

        return $this->privs_cache[$priv][$id_user];
    }

    /**
     * Проверка активности пользователя
     * @param string $id_user		- идентификатор
     * @return bool результат	- true если online
     */
    public function IsOnline($id_user):bool
    {
        if ($this->onlineMap === null) {
            $t = "SELECT DISTINCT id_user FROM  " . SQL_PREFIX . "_sessions";
            $query = sprintf($t, $id_user);
            $result = $this->db->Select($query);

            foreach ($result as $item)
                $this->onlineMap[$item['id_user']] = true;
        }

        return ($this->onlineMap[$id_user] !== null);
    }

    /**
     * Получение id текущего пользователя
     * результат	- UID
     * @return string | null
     */
    public function GetUid()
    {
        // Проверка кеша.
        if ($this->uid !== null) {
            return $this->uid;
        }

        // Берем по текущей сессии.
        $token = $this->GetToken();

        if ($token === null) {
            return null;
        }

        $result = $this->mSessions->one($token);

        // Если сессию не нашли - значит пользователь не авторизован.
        if ($result === null)
            return null;

        // Если нашли - запоминм ее.
        $this->uid = $result['id_user'];
        return $this->uid;
    }

    /**
     * Функция возвращает идентификатор текущей сессии
     * @return string | null результат	- SID
     */
    private function GetToken()
    {
        // Проверка кеша.
        if ($this->token !== null)
            return $this->token;

        // Ищем SID в сессии.
        $token = $_SESSION['sid'];

        // Если нашли, попробуем обновить time_last в базе.
        // Заодно и проверим, есть ли сессия там.
        if ($token !== null) {
            $affected_rows = $this->mSessions->edit(
                $token,
                ['last_modified' => date('Y-m-d H:i:s')]
            );

            if (($affected_rows === 0) && ($this->mSessions->one($token) === null))
                $token = null;
        }

        // Нет сессии? Ищем логин и md5(пароль) в куках.
        // Т.е. пробуем переподключиться.
        if ($token === null && isset($_COOKIE['login'])) {
            $user = $this->GetByLogin($_COOKIE['login']);

            if ($user !== null && $user['pass'] == $_COOKIE['pass'])
                $token = $this->OpenSession($user['id_user']);
        }

        // Запоминаем в кеш.
        if ($token !== null)
            $this->token = $token;

        // Возвращаем, наконец, SID.
        return $token;
    }

    /**
     * Открытие новой сессии
     * @param string $id_user
     * @return string результат	- SID
     */
    private function OpenSession(string $id_user):string
    {
        // генерируем SID
        $token = $this->GenerateStr(10);

        // вставляем SID в БД
        $now = date('Y-m-d H:i:s');
        $session = [];
        $session['id_user'] = $id_user;
        $session['sid'] = $token;
        $session['time_open'] = $now;
        $session['last_modified'] = $now;
        $this->mSessions->add($session);

        // регистрируем сессию в PHP сессии
        $_SESSION['sid'] = $token;

        // возвращаем SID
        return $token;
    }

    /**
     * Генерация случайной последовательности
     * @param int $length 		- ее длина
     * @return string результат	- случайная строка
     */
    private function GenerateStr(int $length = 10):string
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $clen = strlen($chars) - 1;

        while (strlen($code) < $length)
            $code .= $chars[mt_rand(0, $clen)];

        return $code;
    }

    public function get_hash(string $str):string
    {
        return md5($str);
    }

    public function GetPrivs($id_user = null)
    {
        if ($id_user === null)
            $id_user = $this->GetUid();

        if ($id_user === null)
            return array();

        $user = $this->Get($id_user);
        $arr = $this->db->Select("SELECT " . SQL_PREFIX . "_privilege.name as name FROM  " . SQL_PREFIX . "_priv2role LEFT JOIN " .
            SQL_PREFIX . "_privilege USING(id_priv) WHERE id_role = '{$user['id_role']}'");

        $privs = array();
        foreach ($arr as $elem)
            $privs[] = $elem['name'];

        return $privs;
    }

    public function GetRoles()
    {
        $arr = $this->db->Select("SELECT * FROM " . SQL_PREFIX . "_role");
        $roles = array();

        foreach ($arr as $elem)
            $roles[(int)$elem['id_role']] = $elem['description'];

        return $roles;
    }

    public function isAuth():bool
    {
        return !is_null($this->get());
    }
}
