<?php

use core\Response;
use models\User as mUser;
use models\Session as mSession;

$app = include_once 'core/bootstrap.php';

const SQL_PREFIX = 'base';

/**
 * Главная страница
 */
$app->get('/', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html', ['string_var' => 'ssssss'])
        ->addTmp('some_widget.html', ['var' => 5], 'some_widget')
        ->render();
});

/**
 * Форма регистрации
 */
$app->get('/register', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html')
        ->addTmp('register.html', [], 'register')
        ->render();
});

/**
 * Обработка формы регистрации
 */
$app->post('/register', function () use ($app) {
    $post = $app['service.provider']->getService('Request')->post;

    $mUser = new mUser($app['service.provider']->getService('SQL'));

    if ($mUser->register($post['login'], $post['pass'])) {
        (new Response($app))->redirect('/');
    }
});

/**
 * Форма авторизации
 */
$app->get('/login', function () use ($app) {
    (new Response($app))
        ->addTmp('base.html')
        ->addTmp('login.html', [], 'login')
        ->render();
});

/**
 * Обработка формы авторизации
 */
$app->post('/login', function () use ($app) {
    $post = $app['service.provider']->getService('Request')->post;

    $user = $app['service.provider']->getService('User');

    $isAuth = $user->login($post['login'], $post['pass'], $post['remember']);
    $response = new Response($app);

    if (!$isAuth) {
        $response->redirect('/login');
    } else {
        $response->redirect('/');
    }
});

/**
 * Разлогин
 */
$app->get('/logout', function () use ($app) {
    $user = $app['service.provider']->getService('User');

    $user->logout();
    (new Response($app))->redirect('/');

});

$app->get('/only/auth', function () use ($app) {
    $sUser = $app['service.provider']->getService('User');

    if ($sUser->isAuth()) {
        (new Response($app))->redirect('/login');
    }

    echo 'Private page!';
});

$app->get('/privs', function () use ($app) {
    $user = $app['service.provider']->getService('User');

    //var_dump($user->getPrivs());
    //var_dump($user->getRoles());

    $priv = [
        'privs' => [
            'READ' => $user->can('READ'),
            'WRITE' => $user->can('WRITE'),
            'CREATE' => $user->can('CREATE'),
            'MODIFY' => $user->can('MODIFY'),
            'DELETE' => $user->can('DELETE'),
            'BAN' => $user->can('BAN'),
            'BANNED' => $user->can('BANNED')
            ],
        'user' => $user->get()['login']
        ];

    (new Response($app))
        ->addTmp('base.html')
        ->addTmp('privs.html', $priv, 'privs')
        ->render();

});

$app->run();




























